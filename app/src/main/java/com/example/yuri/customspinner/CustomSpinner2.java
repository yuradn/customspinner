package com.example.yuri.customspinner;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by test on 2/16/16.
 */
public class CustomSpinner2 extends RelativeLayout {
    private final static String TAG = "CSpinner";
    private CSpinner spinner;

    public CustomSpinner2(Context context) {
        super(context);
        init();
    }

    public CustomSpinner2(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View v = li.inflate(R.layout.custom_spinner, this, true);
        spinner = (CSpinner) v.findViewById(R.id.spinner);
    }

    public void setSpinner(String[] data, final IOnItemChanged mListener) {
        MyAdapter mAdapter = new MyAdapter(getContext(), android.R.layout.simple_list_item_1, data);

        spinner.setAdapter(mAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mListener != null) mListener.onChange(i);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }

    public class MyAdapter extends ArrayAdapter<String> {

        public MyAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position+1, convertView, parent);
        }

        @Override
        public int getPosition(String item) {
            int pos = super.getPosition(item);
            Log.d(TAG, "getPosition: " + pos);
            return pos+1;
        }

        @Override
        public int getCount() {
            return super.getCount()-1;
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            Log.d(TAG, "Position: "+position+" class: " + parent.getClass().getCanonicalName());
            TextView v = (TextView) super.getView(position, convertView, parent);
            Log.d(TAG, "V: "+v.getClass().getCanonicalName());
            if (position==0) v.setTextColor(Color.GRAY);
            else v.setTextColor(Color.BLACK);
            return v;
        }

    }



    public interface IOnItemChanged {
        void onChange(int id);
    }
}

