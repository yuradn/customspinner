package com.example.yuri.customspinner;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

public class CSpinner extends Spinner {

    public CSpinner(Context context) {
        super(context);
    }

    public CSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CSpinner(Context context, int mode) {
        super(context, mode);
    }

    public CSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setSelection(int position) {
        super.setSelection(position+1);
    }
}
