package com.example.yuri.customspinner;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CustomSpinner2 spinner2 = (CustomSpinner2) findViewById(R.id.sp2);
        spinner2.setSpinner(new String[] {"hint", "one","two","three","four","five"}, null);
    }
}
