package com.example.yuri.customspinner;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

/**
 * Created by test on 2/12/16.
 */
public class CustomSpinner extends Spinner {
    public CustomSpinner(Context context) {
        super(context);
    }

    public CustomSpinner(Context context, int mode) {
        super(context, mode);
        
        init();
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){

    }
}
